//
//  TrashFilterSelector.swift
//  Trashcan_map
//
//  Created by Fiala, Jan on 2/1/23.
//

import SwiftUI

public enum FilterFavourite: String, CaseIterable, Identifiable{
    public var id: String {rawValue}
    
    case ALL = "All"
    case FAVOURITE_ONLY = "Favourite only"
}

struct TrashFilterSelector: View {
    
    @Binding var selected_type: TrashType;
    @Binding var filter_favourite: FilterFavourite;
    
    var body: some View{
        Form{
            Picker("Trash type", selection: $selected_type) {
                ForEach(TrashType.allCases, id: \.id) { value in
                    Text(value.rawValue)
                        .tag(value)
                }
            }
            Picker("Show", selection: $filter_favourite){
                ForEach(FilterFavourite.allCases, id: \.id){ value in
                    Text(value.rawValue)
                        .tag(value)
                }
            }
        }
        .padding()
    }
}

//struct TrashFilterSelector_Previews: PreviewProvider {
//    static var previews: some View {
//       TrashFilterSelector()
//    }
//}
