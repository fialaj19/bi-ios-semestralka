//
//  MapView.swift
//  Trashcan_map
//
//  Created by Fiala, Jan on 1/31/23.
//

import SwiftUI
import MapKit

struct MapView: View {
    
    @State private var region = MKCoordinateRegion(
        center: CLLocationCoordinate2D(latitude: 50.10531, longitude: 14.38978),
        span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
    )
    
    @ObservedObject var trashcan_viewmodel: TrashcansViewModel;
    
    @Binding var nav_path: NavigationPath;
    
    var body: some View {
        ClusteredMapView(trashcans: trashcan_viewmodel.trashcans, region: region, selected_trash_type: $trashcan_viewmodel.selected_trash_type, nav_path: $nav_path, trashcan_viewmodel: trashcan_viewmodel)
            .edgesIgnoringSafeArea(.all)
    }
}

//struct MapView_Previews: PreviewProvider {
//    static var previews: some View {
//        MapView(trashcan_viewmodel: .constant(TrashcansViewModel()))
//    }
//}
