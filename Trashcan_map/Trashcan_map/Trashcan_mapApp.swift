//
//  Trashcan_mapApp.swift
//  Trashcan_map
//
//  Created by Fiala, Jan on 1/31/23.
//

import SwiftUI

@main
struct Trashcan_mapApp: App {
    
    @StateObject private var trashcan_viewmodel: TrashcansViewModel = TrashcansViewModel();
    
    @State var path = NavigationPath()
    
    var body: some Scene {
        WindowGroup {
            NavigationStack(path: $path){
                TabView{
                    if trashcan_viewmodel.screen_state == .LOADING{
                        ProgressView()
                            .progressViewStyle(.circular)
                    }else if trashcan_viewmodel.screen_state == .LOADED{
                            MapView(trashcan_viewmodel: trashcan_viewmodel, nav_path: $path)
                                .tabItem{
                                    Label("Map", systemImage: "map")
                                }
                    }else{
                        VStack{
                            Label("Error", systemImage: "wifi.slash")
                        }
                    }
                    TrashFilterSelector(selected_type: $trashcan_viewmodel.selected_trash_type, filter_favourite: $trashcan_viewmodel.filter_type)
                        .tabItem{
                            Label("Settings", systemImage: "gear")
                        }
                }
                .task {
                    await trashcan_viewmodel.fetch_trashcans()
                }
                .navigationDestination(for: Trashcan.self){ trash in
                    TrashcanDetailView(trashcan: trash, is_favourite: trashcan_viewmodel.is_transcan_favourite(trashcan_id: trash.trashcan_id), trashcan_viewmodel: trashcan_viewmodel)
                }            }
        }
    }
}
