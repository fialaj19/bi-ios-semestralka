//
//  APITrashCans.swift
//  Trashcan_map
//
//  Created by Fiala, Jan on 1/31/23.
//

// Schema for endpoint https://opendata.iprpraha.cz/CUR/ZPK/ZPK_O_Kont_TOitem_b/WGS_84/ZPK_O_Kont_TOitem_b.json
import Foundation
import MapKit

public enum TrashType: String, CaseIterable, Identifiable {
    public var id: String {rawValue}
    
    case PAPER = "Paper"
    case PLASTIC = "Plastic"
    case COLORED_GLASS = "Colored glass"
    case CLEAR_GLASS = "Clear glass"
    case METAL = "Metal"
    case ELECTRICAL_DEVICES = "Electrical devices"
    case UNKNOWN = "Other"
    
    static func from_string(type: String) -> TrashType{
        switch(type){
        case "Papír":
            return TrashType.PAPER;
        case "Plast":
            return TrashType.PLASTIC;
        case "Barevné sklo":
            return TrashType.COLORED_GLASS;
        case "Čiré sklo":
            return TrashType.CLEAR_GLASS;
        case "Kovy":
            return TrashType.METAL;
        case "Elektrozařízení":
            return TrashType.ELECTRICAL_DEVICES
        default:
            return TrashType.UNKNOWN;
        }
        
    }
}
struct Trashcan: Identifiable, Hashable, Equatable{
    let id: String = UUID().uuidString;
    let location: CLLocationCoordinate2D;
    let trashcan_id: Int;
    let station_id: Int;
    let trash_type: TrashType;
    let cleaning_frequency: Int;
    let container_type: String;
    let num_of_containers: Int;
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    static func ==(lhs: Trashcan, rhs:Trashcan) -> Bool{
        return lhs.id == rhs.id
    }
    
    

}

struct APIRootData: Codable{
    let type: String;
    let name: String;
    let features: [APITrashCan];
    
    func to_trashcat_list() -> [Trashcan]{
        var trashcans: [Trashcan] = [];
        
        for api_trashcan in features{
            let trashcan = Trashcan(
                    location: CLLocationCoordinate2D(latitude: api_trashcan.geometry.coordinates[1], longitude: api_trashcan.geometry.coordinates[0]),
                    trashcan_id: api_trashcan.properties.OBJECTID,
                    station_id: api_trashcan.properties.STATIONID,
                    trash_type: TrashType.from_string(type: api_trashcan.properties.TRASHTYPENAME),
                    cleaning_frequency: api_trashcan.properties.CLEANINGFREQUENCYCODE,
                    container_type: api_trashcan.properties.CONTAINERTYPE,
                    num_of_containers: Int(api_trashcan.properties.CONTAINERS) ?? 0)
            trashcans.append(trashcan)
        }
        return trashcans;
    }
        
}

struct APITrashCan: Codable{
    let type: String;
    let geometry: APIPoint;
    let properties: APITrashProperties;
}

struct APIPoint: Codable{
    let type: String;
    let coordinates: [Double];
}

struct APITrashProperties: Codable{
    let OBJECTID: Int;
    let STATIONID: Int;
    let TRASHTYPENAME: String;
    let CLEANINGFREQUENCYCODE: Int;
    let CONTAINERTYPE: String;
    let CONTAINERS: String;
}
