//
//  TrashcanDetailView.swift
//  Trashcan_map
//
//  Created by Fiala, Jan on 2/2/23.
//

import SwiftUI

struct TrashcanDetailView: View {
    var trashcan: Trashcan;
    @State var is_favourite: Bool
    @ObservedObject var trashcan_viewmodel: TrashcansViewModel
    
    var body: some View {
        Form{
            Text(trashcan.container_type)
            Text(trashcan.trash_type.rawValue)
            Text("Number of containers: \(trashcan.num_of_containers)")
            Text("Cleaning frequency: \(trashcan.cleaning_frequency)")
            Button(action: {
                is_favourite.toggle()
                trashcan_viewmodel.set_trashcan_favourite(trashcan_id: trashcan.trashcan_id,favourite: is_favourite)
            }){
                if is_favourite{
                    Label("Favourite", systemImage: "checkmark.square")
                }else{
                    Label("Favourite", systemImage: "square")
                }
            }
        }
    }
}

//struct TrashcanDetailView_Previews: PreviewProvider {
//    static var previews: some View {
//        TrashcanDetailView()
//   }
//}
