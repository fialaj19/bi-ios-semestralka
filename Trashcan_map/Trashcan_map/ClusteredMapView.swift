//
//  ClusteredMapView.swift
//  Trashcan_map
//
//  Created by Fiala, Jan on 2/1/23.
//

import SwiftUI
import MapKit

public func trashTypeToColor(trash_type: TrashType) -> UIColor{
    switch(trash_type){
    case .PAPER:
        return .blue
    case .PLASTIC:
        return .yellow
    case .METAL:
        return .gray
    case .CLEAR_GLASS:
        return .white
    case .COLORED_GLASS:
        return .green
    case .ELECTRICAL_DEVICES:
        return .black
    case .UNKNOWN:
        return .purple
    }
}

struct ClusteredMapView: UIViewRepresentable {

    var trashcans: [Trashcan];
    var region: MKCoordinateRegion;
    @Binding var selected_trash_type: TrashType;
    @Binding var nav_path: NavigationPath;
    @ObservedObject var trashcan_viewmodel: TrashcansViewModel

class Coordinator: NSObject, MKMapViewDelegate{
    
    var parent: ClusteredMapView
    @Binding var selected_trash_type: TrashType;

    init(_ parent: ClusteredMapView, selected_trash_type: Binding<TrashType>) {
        self.parent = parent
        self._selected_trash_type = selected_trash_type
    }
    
    
    
/// showing annotation on the map
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let single_annotation = annotation as? LandmarkAnnotation{
            let annotation_view = AnnotationView(annotation: single_annotation, reuseIdentifier: AnnotationView.ReuseID)
            return annotation_view
        }else if let cluster = annotation as? MKClusterAnnotation{
            let cluster_view = MKMarkerAnnotationView()
            cluster_view.glyphText = String(cluster.memberAnnotations.count)
            cluster_view.markerTintColor = trashTypeToColor(trash_type: selected_trash_type)
            cluster_view.canShowCallout = true
            
            return cluster_view
        }else{
            return nil
        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if let annotation_view = view as? AnnotationView{
            for trash in self.parent.trashcans{
                guard trash.trashcan_id == annotation_view.trashcan_id else{continue}
                
                self.parent.nav_path.append(trash)
            }
        }
    }
}



func makeCoordinator() -> Coordinator {
    ClusteredMapView.Coordinator(self, selected_trash_type: $selected_trash_type)
}


func makeUIView(context: Context) -> MKMapView {
    ///  creating a map
    let view = MKMapView()
    
    /// connecting delegate with the map
    view.delegate = context.coordinator
    view.showsUserLocation = true
    view.setRegion(region, animated: false)
    view.mapType = .standard
    
    addPointsToMapView(view: view, trashcans: trashcans,trashcan_viewmodel: self.trashcan_viewmodel)
    

    return view
    
}
    
    private func addPointsToMapView(view: MKMapView, trashcans: [Trashcan], trashcan_viewmodel: TrashcansViewModel){
    for point in trashcans{
        if point.trash_type != selected_trash_type{
            continue
        }
        if trashcan_viewmodel.filter_type == .FAVOURITE_ONLY{
            if !trashcan_viewmodel.is_transcan_favourite(trashcan_id: point.trashcan_id){
                continue
            }
        }
        let annotation = LandmarkAnnotation(coordinate: point.location, color: trashTypeToColor(trash_type: point.trash_type), title: point.container_type,
                                            trash_type: point.trash_type.rawValue, bin_count: point.num_of_containers, trash_id: point.trashcan_id)
        view.addAnnotation(annotation)
    }
}

func updateUIView(_ uiView: MKMapView, context: Context) {
    uiView.removeAnnotations(uiView.annotations)
    addPointsToMapView(view: uiView, trashcans: trashcans, trashcan_viewmodel: self.trashcan_viewmodel)
}
}


class LandmarkAnnotation: NSObject, MKAnnotation {
let coordinate: CLLocationCoordinate2D
    let title: String?
    let color: UIColor
    let trash_type: String
    let bin_count: Int
    let trash_id: Int;
init(
     coordinate: CLLocationCoordinate2D,
     color: UIColor,
     title: String,
     trash_type: String,
     bin_count: Int,
     trash_id: Int
) {
    self.coordinate = coordinate
    self.title = title
    self.trash_id = trash_id
    self.color = color
    self.trash_type = trash_type
    self.bin_count = bin_count
    super.init()
}
}


/// here posible to customize annotation view
let clusterID = "clustering"

class AnnotationView: MKMarkerAnnotationView {

static let ReuseID = "cultureAnnotation"
    
    let trashcan_id: Int;

/// setting the key for clustering annotations
    override init(annotation: MKAnnotation?, reuseIdentifier: String?){
        let annotation = annotation as? LandmarkAnnotation
        self.trashcan_id = annotation!.trash_id
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        self.markerTintColor = annotation!.color
        clusteringIdentifier = clusterID
        self.tintColor = annotation!.color
        self.glyphImage = UIImage(systemName: "trash")
    }

required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
}

override func prepareForDisplay() {
    super.prepareForDisplay()
    displayPriority = .defaultLow
 }
}
