//
//  TrashcansViewModel.swift
//  Trashcan_map
//
//  Created by Fiala, Jan on 1/31/23.
//

import Foundation
import MapKit
import SwiftUI

enum ScreenState{
    case LOADING, LOADED, ERROR;
}

class TrashcansViewModel: ObservableObject{
    @Published var trashcans: [Trashcan];
    @Published var screen_state: ScreenState;
    @Published var selected_trash_type: TrashType;
    @Published var filter_type: FilterFavourite;
    
    var favourite_trashcans: Set<Int>;
    let favourite_trashcans_key = "favourite_trashcans"
    
    init() {
        self.trashcans = []
        self.screen_state = .LOADING
        self.selected_trash_type = .PAPER
        self.filter_type = .ALL
        
        
        if let favourite_trashcans_object = UserDefaults.standard.object(forKey: favourite_trashcans_key){
            if let favourite_trashcans = favourite_trashcans_object as? Array<Int>{
                self.favourite_trashcans = Set(favourite_trashcans)
            }else{
                self.favourite_trashcans = []
            }
        }else{
            self.favourite_trashcans = []
        }
        
    }
    
    @MainActor
    func fetch_trashcans() async{
        var request = URLRequest(url: URL(string: "https://opendata.iprpraha.cz/CUR/ZPK/ZPK_O_Kont_TOitem_b/WGS_84/ZPK_O_Kont_TOitem_b.json")!);
        
        request.httpMethod = "GET";
        request.timeoutInterval = 20;
        
        do{
            let (data, _) = try await URLSession.shared.data(for: request);
            let api_data: APIRootData = try JSONDecoder().decode(APIRootData.self, from: data);
            self.trashcans = api_data.to_trashcat_list();
            screen_state = .LOADED;
        }catch{
            screen_state = .ERROR;
            print("[ERROR]", error)
        }
    }
    
    func set_trashcan_favourite(trashcan_id: Int, favourite: Bool){
        if favourite{
            self.favourite_trashcans.insert(trashcan_id)
        }else{
            self.favourite_trashcans.remove(trashcan_id)
        }
        save_favourite_trashcans()
    }
    
    func save_favourite_trashcans(){
        Task{
            UserDefaults.standard.set(Array(self.favourite_trashcans), forKey: self.favourite_trashcans_key)
        }
    }
    
    func is_transcan_favourite(trashcan_id: Int) -> Bool{
        return self.favourite_trashcans.contains(trashcan_id)
    }
}
